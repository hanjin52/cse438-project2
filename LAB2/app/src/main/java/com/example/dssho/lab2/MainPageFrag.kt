package com.example.dssho.lab2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_toplist.*
import kotlinx.android.synthetic.main.fragment_toplist.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

/**
 * Frag class for main page
 */
class MainPageFrag() : Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val myView: View = inflater.inflate(R.layout.fragment_toplist, container, false)
        val greeting_message =
            arrayListOf<String>(
                "Welcome to explore Last.FM!",
                "Welcome! Billions of songs are here",
                "Hope you're having a nice day. Enjoy last.fm",
                "WELCOME! Enjoy last.fm!",
                "How are you? Music?"
            )

        val randomNum = Random()
        val randomNumber = randomNum.nextInt(greeting_message.count())
        val queryField = myView.findViewById<EditText>(R.id.queryText)
        queryField.setHint(greeting_message[randomNumber])
        myView.searchButton.setOnClickListener { view ->
            val queryTex = myView.findViewById<EditText>(R.id.queryText).text.toString()


            if (queryTex == "") {
                Toast.makeText(context, "Please enter something!", Toast.LENGTH_SHORT).show()
            } else {

                val url =
                    "http://ws.audioscrobbler.com/2.0/?method=track.search&track=" + queryTex + "&api_key=9fa035aaecca59e3f897d27cd4de59d8&format=json"
                AsyncTaskHandleJson().execute(url)
            }

        }
        myView.track_list.setOnItemClickListener { parent, view, position, id ->

            for (x in 0 until 100) {
                if (position == x) {
                    Log.e("okay", "okay")
                    val text = parent.getAdapter().getItem(position) as Track
                    val trackName = text.politic
                    val trackCover = text.time
                    val trackURL = text.traURL
                    val playcount = text.numPlayed
                    val listenersNum = text.listeners
                    val art = text.artist
                    val intent: Intent = Intent(context, TrackDetail::class.java)
                    intent.putExtra("totalCal", trackName)
                    intent.putExtra("trackCover", trackCover)
                    intent.putExtra("trackURL", trackURL)
                    intent.putExtra("numPlayed", playcount)
                    intent.putExtra("listenersNum", listenersNum)
                    intent.putExtra("art", art)
                    startActivity(intent)
                }
            }

        }
        return myView
    }

    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg url: String?): String {
            val text: String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use {
                    it.reader().use { reader -> reader.readText() }
                }

            } finally {
                connection.disconnect()
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handlejson(result)
        }


        private fun handlejson(jsonString: String?) {
            val jsonooo = JSONObject(jsonString)
            val newlis = jsonooo.getString("results")
            val jsono = JSONObject(newlis)
            val newlist = jsono.getString("trackmatches")
            val jsonoo = JSONObject(newlist)
            val finalist = jsonoo.getString("track")
            val jsonArray = JSONArray(finalist)
            val list = ArrayList<Track>()
            var index = 0
            while (index < jsonArray.length()) {

                val jsonObject = jsonArray.getJSONObject(index)

                val imageList = jsonObject.getString("image")
                val image = JSONArray(imageList)
                val imgURL = JSONObject(image[2].toString())
                val art = jsonObject.getString("artist")
                list.add(
                    Track(
                        jsonObject.getString("name").trim(), imgURL.getString("#text").toString(),
                        "", jsonObject.getString("url"), jsonObject.getString("listeners").trim(), art
                    )
                )

                index++
            }
            val adapter = ListAdapter(context!!, list)
            track_list.adapter = adapter

        }
    }

    inner class ListAdapter(val context: Context, var list: ArrayList<Track>) : BaseAdapter() {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            val view = LayoutInflater.from(context).inflate(R.layout.row_list, parent, false)
            val songName = view.findViewById(R.id.trackTitle) as AppCompatTextView
            val coverImage = view.findViewById<ImageView>(R.id.imageTrack)
            songName.text = list[position].politic
            Picasso.get().load(list[position].time).into(coverImage)
            return view
        }

        override fun getItem(position: Int): Any {

            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }

    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {

        fun onFragmentInteraction(uri: Uri)
    }
}
