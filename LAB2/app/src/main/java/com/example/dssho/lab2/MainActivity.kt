package com.example.dssho.lab2

import android.content.Context
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_toplist.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter
        tabs_main.setupWithViewPager(viewpager_main)






       val url = "http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=9fa035aaecca59e3f897d27cd4de59d8&format=json"
        AsyncTaskHandleJson().execute(url) //fetching data from the web
        val itemView = layoutInflater.inflate(R.layout.fragment_toplist, null)
    }

    //Fetching json data
    inner class AsyncTaskHandleJson: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val jdata:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try{
                connection.connect()
                jdata = connection.inputStream.use{
                    it.reader().use{reader->reader.readText()}
                }

            }finally {
                connection.disconnect()
            }
            return jdata
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handlejson(result)
        }

        //Parsing Json
        private fun handlejson(jsonString: String?) {
            val jsono = JSONObject(jsonString)
            val newlist = jsono.getString("tracks")
            val jsonoo = JSONObject(newlist)
            val finalist = jsonoo.getString("track")
            val jsonArray = JSONArray(finalist)
            //Iterate JSON Array
            val list = ArrayList<Track>()
            var x =0
            while(x<jsonArray.length()){
                val jsonObject = jsonArray.getJSONObject(x)
                val imageList = jsonObject.getString("image")
                val image = JSONArray(imageList)
                val imgURL = JSONObject(image[2].toString())
                val artistList =  jsonObject.getString("artist")
                val art = JSONObject(artistList).getString("name")
        list.add(
                    Track(
                        jsonObject.getString("name").trim(),imgURL.getString("#text").toString(),
                        jsonObject.getString("playcount").trim(),jsonObject.getString("url"),jsonObject.getString("listeners").trim(),art
                    ))

                x++
            }
            val adapter = ListAdapter(this@MainActivity,list)
            track_list.adapter = adapter

        }
    }
    //list adapter
    inner class ListAdapter(val context:Context, var list:ArrayList<Track>): BaseAdapter(){
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = LayoutInflater.from(context).inflate(R.layout.row_list,parent,false)
            val trackT = view.findViewById(R.id.trackTitle) as AppCompatTextView
            val songIMG = view.findViewById<ImageView>(R.id.imageTrack)
            trackT.text=list[position].politic
            Picasso.get().load(list[position].time).into(songIMG)
            return view
        }

        override fun getItem(position: Int): Any {

            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }

    }

    // Set tab view, with reference to JokeDB
    class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    MainPageFrag()
                }
                else -> TrackList()
            }
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Music"
                else -> {
                    return "Playlist"
                }
            }
        }
        override fun getCount(): Int {
            return 2
        }
    }
}
