package com.example.dssho.lab2.db

import android.provider.BaseColumns

class DbSettings {
    companion object {
        const val DB_NAME = "tracks.db"
        const val DB_VERSION = 1
    }

    class DBTrackEntry: BaseColumns {
        companion object {
            const val TABLE = "tracks"
            const val ID = BaseColumns._ID
            const val COL_TRACK = "setup"
            const val COL_REMARK = "remarks"
        }
    }
}