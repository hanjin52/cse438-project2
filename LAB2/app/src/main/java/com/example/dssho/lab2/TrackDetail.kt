package com.example.dssho.lab2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.dssho.lab2.db.DatabaseHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_track_detail.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

//Page for each track
class TrackDetail : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_detail)
        //  val url ="http://ws.audioscrobbler.com/2.0/?method=track.search&track=Believe&api_key=9fa035aaecca59e3f897d27cd4de59d8&format=json"

        val profileName = intent.getStringExtra("totalCal").toString()
        val trackCover = intent.getStringExtra("trackCover")
        val art = intent.getStringExtra("art").toString()
        val myUrl = intent.getStringExtra("trackURL")
        val playcount = intent.getStringExtra("numPlayed")
        val listenerNum = intent.getStringExtra("listenersNum")
        Picasso.get().load(trackCover).into(trCover)

        val url =
            "http://ws.audioscrobbler.com/2.0/?method=track.getsimilar&artist=" + art + "&track=" + profileName + "&api_key=9fa035aaecca59e3f897d27cd4de59d8&format=json"

        AsyncTaskHandleJson().execute(url)
        trackName.setText(profileName)
        songTitle.setText(profileName)

        play.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(myUrl.toString())
            startActivity(i)
        }
        trArtist.setText(art)
        textView2.setText("Play count: " + playcount)
        textView3.setText("Listeners: " + listenerNum)

        simTrack.setOnItemClickListener { parent, view, position, id ->
            for (x in 0 until 100) {
                if (position == x) {
                    val text = parent.getAdapter().getItem(position) as Track
                    val trackName = text.politic
                    val trackCover = text.time
                    val trackURL = text.traURL
                    val playcount = text.numPlayed
                    val listenersNum = text.listeners
                    val art = text.artist

                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(this, trackURL, duration)
                    toast.show()
                    val intent: Intent = Intent(this, TrackDetail::class.java)
                    intent.putExtra("totalCal", trackName)
                    intent.putExtra("trackCover", trackCover)
                    intent.putExtra("trackURL", trackURL)
                    intent.putExtra("numPlayed", playcount)
                    intent.putExtra("listenersNum", listenersNum)
                    intent.putExtra("art", art)
                    startActivity(intent)

                }
            }

        }


        runButton.setOnClickListener { view ->
            val db = DatabaseHelper(this)
            db.addTrack(songTitle.text.toString(), songRemarks.text.toString())
            songTitle.text.clear()
            songRemarks.text.clear()
            val text = "Track Added!"
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(this, text, duration)
            toast.show()
        }


    }

    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg url: String?): String {
            val text: String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use {
                    it.reader().use { reader -> reader.readText() }
                }

            } finally {
                connection.disconnect()
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handlejson(result)
        }


        private fun handlejson(jsonString: String?) {
            val jsono = JSONObject(jsonString)
            val newlist = jsono.getString("similartracks")
            val jsonoo = JSONObject(newlist)
            val finalist = jsonoo.getString("track")
            val jsonArray = JSONArray(finalist)
            val list = ArrayList<Track>()
            var x = 0
            while (x < jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(x)
                val imageList = jsonObject.getString("image")
                val image = JSONArray(imageList)
                val imgURL = JSONObject(image[2].toString())
                val artistList = jsonObject.getString("artist")
                val art = JSONObject(artistList).getString("name")
                list.add(
                    Track(
                        jsonObject.getString("name").trim(), imgURL.getString("#text").toString(),
                        jsonObject.getString("playcount").trim(), jsonObject.getString("url"), "unknown", art
                    )
                )

                x++
            }
            val adapter = ListAdapter(this@TrackDetail, list)
            simTrack.adapter = adapter

        }
    }

    inner class ListAdapter(val context: Context, var list: ArrayList<Track>) : BaseAdapter() {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val myView = LayoutInflater.from(context).inflate(R.layout.similar_list, parent, false)
            val presidentPolitic = myView.findViewById(R.id.president_politic) as AppCompatTextView
            presidentPolitic.text = list[position].politic
            return myView
        }

        override fun getItem(position: Int): Any {

            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }

    }
}
