package com.example.dssho.lab2.ViewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.example.dssho.lab2.db.DbSettings
import com.example.dssho.lab2.model.Track
import com.example.dssho.lab2.db.DatabaseHelper
import android.arch.lifecycle.MutableLiveData
import android.provider.BaseColumns


class TrackViewModel(application: Application): AndroidViewModel(application) {
    private var _trackList: MutableLiveData<ArrayList<Track>> = MutableLiveData()
    private var _tracksDBHelper: DatabaseHelper = DatabaseHelper(application)

    fun gettracks(): MutableLiveData<ArrayList<Track>> {
        loadtracks()
        return _trackList
    }

    private fun loadtracks() {
        val db = _tracksDBHelper.readableDatabase

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        val projection = arrayOf(BaseColumns._ID, DbSettings.DBTrackEntry.COL_TRACK, DbSettings.DBTrackEntry.COL_REMARK)

        val cursor = db.query(
            DbSettings.DBTrackEntry.TABLE,
            projection,
            null,
            null,
            null,
            null,
            null
        )

        val tracks = ArrayList<Track>()
        with(cursor) {
            while (moveToNext()) {
                val setup = getString(getColumnIndexOrThrow(DbSettings.DBTrackEntry.COL_TRACK))
                val punchline = getString(getColumnIndexOrThrow(DbSettings.DBTrackEntry.COL_REMARK))
                val j = Track(setup, punchline)
                tracks.add(j)
            }
        }
        _trackList.value = tracks
    }
}