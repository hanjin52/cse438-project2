package com.example.dssho.lab2.db
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

//Database helper class
class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + DbSettings.DBTrackEntry.TABLE + " ( " +
                DbSettings.DBTrackEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbSettings.DBTrackEntry.COL_TRACK + " TEXT NULL, " +
                DbSettings.DBTrackEntry.COL_REMARK + " TEXT NULL)"
        db?.execSQL(createFavoritesTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBTrackEntry.TABLE)
        onCreate(db)
    }

    //add tracks
    fun addTrack(track: String, remark: String) {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(DbSettings.DBTrackEntry.COL_TRACK, track)
            put(DbSettings.DBTrackEntry.COL_REMARK, remark)
        }
        val newRowId = db?.insert(DbSettings.DBTrackEntry.TABLE, null, values)
    }

    //remove tracks
    fun removeTrack(track: String, punchline: String) {
        val db = this.writableDatabase
        val newRow = db?.delete(
            DbSettings.DBTrackEntry.TABLE,
            DbSettings.DBTrackEntry.COL_TRACK + "=?",
            arrayOf(track.toString())
        )

    }
}