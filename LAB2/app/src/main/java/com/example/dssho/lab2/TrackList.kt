package com.example.dssho.lab2

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.dssho.lab2.ViewModel.TrackViewModel
import com.example.dssho.lab2.db.DatabaseHelper
import com.example.dssho.lab2.model.Track
import kotlinx.android.synthetic.main.fragment_play_list.*
import kotlinx.android.synthetic.main.track_item.view.*


/**
 * Playlist
 */
@SuppressLint("ValidFragment")
class TrackList() : Fragment() {
    private var adapter = TrackAdapter()
    private lateinit var viewModel: TrackViewModel
    private var trackList: ArrayList<Track> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_play_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        result_items_list.layoutManager = LinearLayoutManager(this.context)
        result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(TrackViewModel::class.java)

        val observer = Observer<ArrayList<Track>> {
            result_items_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if (p0 >= trackList.size || p1 >= trackList.size) {
                        return false
                    }
                    return trackList[p0].tracks == trackList[p1].tracks
                }

                override fun getOldListSize(): Int {
                    return trackList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return trackList[p0] == trackList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            trackList = it ?: ArrayList()
        }
        viewModel.gettracks().observe(this, observer)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            fragmentManager!!.beginTransaction().detach(this).attach(this).commit()
        }
    }

    inner class TrackAdapter : RecyclerView.Adapter<TrackAdapter.TrackViewHolder>() {
        //View holder for playlist
        inner class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var trackName: TextView = itemView.songTitle
            var personalRemarks: TextView = itemView.songRemarks
        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TrackViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.track_item, p0, false)
            val rmButton = itemView.findViewById<Button>(R.id.removeTrack)
            val trackName = itemView.findViewById<TextView>(R.id.songTitle)
            val playThis = itemView.findViewById<Button>(R.id.playThis)

            //redirect to last.fm page to play that music
            playThis.setOnClickListener {
                val query = trackName.text
                val myUrl = "https://www.last.fm/search?q=" + query
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(myUrl)
                startActivity(i)
            }
            rmButton.setOnClickListener { view ->
                val db = DatabaseHelper(context!!)
                db.removeTrack(trackName.text.toString(), "remove")
                val text = "The track has been removed, you would no longer see it anymore"
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(context, text, duration)
                toast.show()
            }
            return TrackViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: TrackViewHolder, p1: Int) {
            val joke = trackList[p1]
            p0.trackName.text = joke.tracks
            p0.personalRemarks.text = joke.remarks
        }

        override fun getItemCount(): Int {
            return trackList.size
        }

    }
}
