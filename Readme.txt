# Randomly generated welcoming message
- At the start of the APP, a random message is displayed at the search field
- Improve the interactivity of the app. Make this APP look smart in the eyes of the end user
- Implemented random methods and setHint()

#Play the track on its page
- User can play the track by clicking the button 'play' and this would redirect user to the corresponding page at Last.FM
- Lots of user want to access the track so we fulfill their need by redirecting them to the Last.FM page.
- Get the url retrieved from the json data then add it with the button.

#Similar tracks with links to their pages
- User can view similar tracks at the detail page. Each track is clickable and will link to its corresponding page in this APP.
- This feature enables user to discover new songs they would like and makes this APP look intelligent
- Implemented using getSimilar API. And add listeners to each item with link to the detail page.
- Note that for some latest tracks, the data returned by API is null.You can click the track 7 rings to test it.

#Personal remarks
- User can write some personal notes when adding a track to playlist[optional].
- Bring personalized experience to user. Make this app unique.
- Add another field to the SQLite database and set corresponding editText,listeners, etc.

#Play the track on playlist
- User can play the track on playlist by clicking the button 'play', and this will redirect user to the corresponding page at Last.FM
- Users wanna play their favorites! So we bring this to them via Last.FM

(10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
	Track is not removed right away, you must reload the playlist (-2)
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(10 / 15 points) Creative portion: Be creative!
	I know that you implemented a few different things here, but they all seem fairly trivial. Two of your features are basically the same thing, and when you say "play"
	the music, I think that is a little misleading. It is a button that opens a browser. The random welcome message is nice but also trivial. The most impressive improvement
	was the ability to add comments to a track, which was done well, but not enough for the full 15 points.
	
Total: 93 / 100